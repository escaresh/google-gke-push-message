package com.humberto.gke.google_pub_sub_app.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.humberto.gke.google_pub_sub_app.GooglePubSubAppApplication.PubsubOutboundGateway;
import com.humberto.gke.google_pub_sub_app.models.Usuario;


@RestController
public class GooglePubSubController {

	@Autowired
	PubsubOutboundGateway messageGatewuy;

	@PostMapping("/publish")
	public String publishMessage(@RequestBody MyAppGCPMessage message) {
		messageGatewuy.sendToPubsub(message.toString());
		return "Message published to Google Pub/Sub successfully";
	}
	
	@PostMapping("/usuario")
	public Usuario usuario(@RequestBody Usuario usuario) {
		return usuario;
	}
}
